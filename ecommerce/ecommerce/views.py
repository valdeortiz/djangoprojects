from django.shortcuts import render
from django.http import HttpResponse


def hola(request):
    return render(request,'index.html',{
        'msg': 'mensaje',
	'products': [
		{'name':'lola',
        	'price': 5,
        	'stock': False},
		{'name':'sasd',
        	'price':  89,
        	'stock':  True},
		{'name':'lolsaasa',
        	'price': 5,
        	'stock': True}
	]
    })
