# django imports
from django.shortcuts import render, redirect

# modelos
from .models import Persona

from .form import PersonaForm

def inicio(request):
	personas = Persona.objects.all()
	context = {
		'personas': personas
	}
	return render(request, 'index.html', context)

def crear_persona(request):
	if request.method == 'GET':
		form = PersonaForm()
	else:
		form = PersonaForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('home')
	
	context = {
		'form': form
	}
	
	return render(request, 'crearPersona.html',context)
	
def editar_persona(request, id):
	persona = Persona.objects.get(id=id)
	if request.method == 'GET':
		form = PersonaForm(instance = persona)
	else:
		form = PersonaForm(request.POST, instance=persona)
		if form.is_valid():
			form.save()
			return redirect('home')

	context = {
		'form': form
	}

	return render(request, 'crearPersona.html', context)
	
def eliminar_persona(request, id):
	persona = Persona.objects.get(id=id)
	persona.delete()
	
	return redirect('home')