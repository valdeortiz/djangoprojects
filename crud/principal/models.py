# django imports
from django.db import models

# cada modelo es una tabla en mi db

class Persona(models.Model):
	nombre = models.CharField(max_length=100)
	apellido = models.CharField(max_length=100)
	correo = models.EmailField(max_length=200)

	def __str__(self):
		"""Mensaje del modelo"""
		return self.nombre