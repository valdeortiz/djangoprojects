# django imports
from django.shortcuts import render, redirect
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from django.urls import reverse_lazy

# formularios
from .form import PersonaForm

# modelos
from .models import Persona

"""
la clase padre de las  vistas basadas en clases
class view():
	def dispatch: verificar el metodo http
	http_not_allowed

	def get_queryset()
		return self.object.all()
	
	def get_context_data():
		context = {}
		context['queryset'] = self.get_queryset()
		return context

	def get_template_name():
		return self.template_name

	def get(self.request):
		return render(request, self.get_template_name, self.queryset)
"""

class PersonaList(ListView):
	model = Persona
	template_name = 'index.html'

class PersonaCreate(CreateView):
	model = Persona
	form_class = PersonaForm
	template_name = 'crearPersona.html'
	success_url = reverse_lazy('home')

class PersonaUpdate(UpdateView):
	model = Persona
	form_class = PersonaForm
	template_name = 'crearPersona.html'
	success_url = reverse_lazy('home')

class PersonaDelete(DeleteView):
	model = Persona
	template_name = 'verificar.html'
	success_url = reverse_lazy('home')
	