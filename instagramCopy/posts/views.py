
#django
from django.contrib.auth.mixin import LoginRequiredMixin
from django.views.generic import ListView
from django.url import reverse_lazy
#forms
from posts.forms import PostForm

#models
from posts.models import Post

class createPostView(LoginRequiredMixin, ListView):
	template_name = 'posts/new.html'
	form_class = PostForm
	success_url = reverse_lazy('posts:feed')
	
	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['user'] = self.request.user
		context['profile'] = self.request.user.profile
		return context

class PostsFeedView(LoginRequiredMixin, ListView):
	""" Return all plublished posts"""
	template_name = 'posts/feed.html'
	model = Post
	ordering = ('-created',)
	paginate_by = 30
	context_object_name = 'posts'

class PostsDetailView(LoginRequiredMixin, ListView):
	""" Return post detail."""
	template_name = 'posts/detail.html'
	queryset = Post.objects.all()
	context_object_name = 'post'

