

from django.contrib import admin

from posts.models import Post

# Register your models here.
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
	list_display = ('pk','user', 'title', 'photo')
	list_display_links = ('pk', 'user')
	
	search_fields = (
		'user__email', 
		'user__username',
		'user__first_name',
		'user__last_name', 
	)
	list_filter = (
		'created', 
		'modify', 
		'user__is_active', 
		'user__is_staff'
	)
	fieldsets = (
		('Profile', {
			'fields':(
				('user'),
			),
		}),
		('Extra info', {
			'fields': (
				('title', 'photo'), 
				
			)
		}),
		('Metadata', {
			'fields': (('created', 'modify')),
		}),
	) 
	readonly_fields = ('created', 'modify')