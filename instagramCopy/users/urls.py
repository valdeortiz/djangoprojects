

#django
from django.urls import path

#view
from users import views

urlpatterns = [
	

	#Management
    path(
    	route='login/',
    	view=users_views.LoginView.as_view(),
    	name='login'
    ),
    path(
    	route='logout/',
    	view=users_views.LogoutViews.as_view(),
    	name='logout'
    ),
    path(
    	route='signup/', 
    	view=users_views.SignupView.as_view(), 
    	name='signup'
    ),

    path(
    	route='me/profile', 
    	view=users_views.UpdateProfileView.as_view(), 
    	name='update_profile'
    ),

	#posts, colocamos al final para que las otras tengan prioridad y si las url son parecidas esta sea la ultima en tomar
	path(
		route='<str:username>/',
		view=view.UserDetailView.as_view(),
		name='detail'
	),
]