"""users views"""

# Django
from django.contrib.auth.mixin import LoginRequiredMixin
from django.contrib.auth import views as auth_views
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.view.generic import UpdateView, DetailView, FormView


# Models
from django.contrib.auth.models import User
from post.models import Posts
from users.models import Profile

# Forms
from users.forms import SignupForm


# Create your views here.

class UserDetailView(LoginRequiredMixin,DetailView):

	template_name = 'users/detail.html'
	slug_field = 'username'
	slug_url_kwarg = 'username' # este es la variable que recibimos en la url
	queryset = User.objects.all()
	context_object_name = 'user'

	def get_context_date(self, **kwargs):
		context = super().get_context_date(**kwargs)
		user = self.get_object()
		context['posts'] = Posts.objects.filter(user=user).order_by('-created')
		return context

class SignupView(FormView):
	"""user sign up view """
	template_name = 'users/signup.html'
	form_class = SignupForm
	succes_url = reverse_lazy('users:login')

	def form_valid(self, form):
		"""save form data."""
		form.save()
		return super().form_valid(form)

class UpdateProfileView(LoginRequiredMixin, UpdateView):
	"""update profile view """
	template_name = 'users/update_profile.html'
	model = Profile
	fields = ['website', 'biography', 'phone_number', 'picture']

	def get_object(self):
		"""return user Profile"""
		return self.request.user.profile

	def get_succes_url(self):
		"""return to user profile"""
		return self.object.user.username
		return reverse('users:detail', kwargs={'username':username})

class LoginView(auth_views.LoginView):
	""" login view"""
	template_name = 'users/login.html'

class LogoutView(LoginRequiredMixin, auth_views.LogoutView)
	"""logout view""" 
	template_name = 'users/logged_out.html'
